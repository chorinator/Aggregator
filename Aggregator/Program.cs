﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.IO;
using Newtonsoft.Json.Linq;
using System.Reflection;
using MessageBrokers.Models.Consumers;
using MessageBrokers.Models.Request;
using MessageBrokers.Models.WorkDispatcher;
using MessageBrokers.Models.Work;
using MessageBrokers.Models.Producers;
using MessageBrokers.Models.WorkResult;
using MessageBrokers.Models.WorkListener;
using MessageBrokers.Models.Storage;

namespace Aggregator
{
    class Program
    {
        private static readonly JObject config = GetConfiguration();

        static void Main(string[] args)
        {
            Console.WriteLine("Waiting to aggregate...");

            var tasks = new List<Task>();

            //Aggregator
            tasks.Add(CreateAggregator());

            Task.WaitAll(tasks.ToArray());
        }

        private static Task CreateAggregator()
        {
            var prefix = "Aggregator";

            var queuename = config[prefix]["QueueName"].ToString();
            var listener = new AggregatorListener(CreateConsumer(prefix, queuename));
            var processor = new QuoteAggregator<APIRequest>(60000, (RabbitMQListener<APIRequest>)CreateConsumer(prefix, "Random"), new RedisStorage());
            var publisher = new PublishResultToRabbitMQ((RabbitMQPublisher)CreateProducer(prefix, ""), "aggregator.response", RabbitMQPublishOptions.Queue);
            var dispatcher = new AggregatorDispatcher<APIRequest>(listener, processor, publisher, new RedisStorage());
            return dispatcher.DispatchAsync();
        }

        private static JObject GetConfiguration()
        {
            var assembly = Assembly.GetExecutingAssembly();
            var resourceName = "Aggregator.credentials.json";
            JObject o;
            using (Stream stream = assembly.GetManifestResourceStream(resourceName))
            using (StreamReader reader = new StreamReader(stream))
            {
                string result = reader.ReadToEnd();
                o = JObject.Parse(result);
            }

            return o;
        }

        private static IMessageProducer CreateProducer(string prefix, string queue)
        {
            return new RabbitMQPublisher(
                        config[prefix]["Hostname"].ToString(),
                        config[prefix]["Username"].ToString(),
                        config[prefix]["Password"].ToString())
            {
                QueueName = queue,
                PublishTo = RabbitMQPublishOptions.Queue,
            };
        }

        private static IMessageConsumer CreateConsumer(string prefix, string queue)
        {
            return new RabbitMQSynchronousListener<APIRequest>(
                        config[prefix]["Hostname"].ToString(),
                        config[prefix]["Username"].ToString(),
                        config[prefix]["Password"].ToString())
            {
                QueueName = queue,
                DeclareQueue = false,
            };
        }
    }
}
